package net.techu.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("clientesDiegoLopez")
public class Persona {

    @Id
    public String id;
    public String nombre;
    public String apellido;
    public String ciudad;

    public Persona(String id, String nombre, String apellido, String ciudad) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.ciudad = ciudad;
    }

}
